data = load(fullfile("ExemplaryTimeSeries/TimeSeries_WholeRun_anteriorInsula_right_probabilistic_sham_8604.txt"));
x = data(:,1);

figure;
spectrogram(x, 10, 9,[],0.5, 'yaxis', 'psd')
yline(100,'--')
yline(10,'--')
fontsize(gca,20,'pixels')
ylim([0,250])
a = colorbar();
ylabel(a,'Power/Frequency (dB/Hz)')
saveas(gcf, 'Spectogram_sham_antInsR_8604.svg', 'svg')