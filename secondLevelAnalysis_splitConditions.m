%% 
clear all
secondLevelPairedtTest = 1; 
secondLevelOneSampleTTest = 0;

seedDirPath = fullfile("C:","EXAMPLEPATH","masks");
seedDir = dir(fullfile(seedDirPath));

seedList = {};
session = 'poststimulation';

if strcmp(session, 'baseline')
    contrastN = '1';
elseif strcmp(session, 'stimulation')
    contrastN = '3';
else
    contrastN = '5';
end
        

% for i = 1:length(seedDir)
%     seed = seedDir(i).name;
%     if length(seed) > 2 
%         seedList{end + 1} = seed;
%     end
% end

% if seedList{end} == 'Others'
% seedList(:,17) = [];
% end



folderPath = fullfile("C:","EXAMPLEPATH","FirstLevel","SplitConditions_combinedROIs_WeightedMean_Butterworth6", "anteriorInsula_left_probabilistic");
folder = dir(fullfile(folderPath));

subjects = {};
conditions = {'sham', 'stim'};

for i = 1:length(folder)
    subject = folder(i).name;
    if length(subject) == 4 
        subjects{end + 1} = subject;
    end
end

seedList = {'posteriorInsula_right_probabilistic', 'anteriorInsula_left_probabilistic', 'anteriorInsula_right_probabilistic', 'posteriorInsula_left_probabilistic'};
% seedList = {'anteriorInsula_right_binarizedAfterProbabilistic'};


% delete motion subject (and right now also the one without any files)
% subjects(:,5) = [];
% subjects(:,13) = [];



%% 
for i = 1:length(seedList)
   seedDirName = seedList{i};
   seed_name = seedDirName;
    if secondLevelPairedtTest
        kindOfAnalysis = 'PairedtTest_ShamStim_combinedROIs_WeightedMean_Butterworth6'
%         for j = 1:2
%             whichSide = '';
%             if j == 1
%                 whichSide = 'left';
%             else
%                 whichSide = 'right';
%             end
%             seed_name = sprintf('%s_%s', seedDirName, whichSide);
%             seed_name = 'anteriorInsula_right_binarized'
            
            secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", 'Split Condition', kindOfAnalysis, seed_name, session);
%             secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", kindOfAnalysis, seedDirName, seed_name);

            cd(fullfile("C:","EXAMPLEPATH","Code","tVNS")
            load('SecondLevel_PairedtTest_ShamStim.mat')

            matlabbatch{1, 1}.spm.stats.factorial_design.dir = cellstr(secondleveldir);
            for k = 1:length(subjects)
%                 firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'WholeRun_ProbabilisticSeeds', seedDirName, seed_name, subjects{k});
                firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'SplitConditions_combinedROIs_WeightedMean_Butterworth6', seed_name, subjects{k});

                matlabbatch{1, 1}.spm.stats.factorial_design.des.pt.pair(k).scans(1,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'sham'), sprintf('^con.*0%s', contrastN)));
                matlabbatch{1, 1}.spm.stats.factorial_design.des.pt.pair(k).scans(2,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'stim'), sprintf('^con.*0%s', contrastN)));
            end
            spm_jobman('run',matlabbatch);
            clear matlabbatch img


            load('EstimateFirstLevel.mat')
            matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(secondleveldir, "SPM.mat"));
            spm_jobman('run',matlabbatch);
            clear matlabbatch img
            
            matlabbatch = [];
            matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(secondleveldir, "SPM.mat"));
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'sham-stim'
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1 -1];
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = 'stim-sham';
            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [-1 1];
            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';

            spm_jobman('run', matlabbatch);
%         end
    end
    if secondLevelOneSampleTTest
        kindOfAnalysis = 'OneSampleTTest_ShamStim_CombinedROIs_WeightedMean_Butterworth6'
%         for j = 1:2
%             whichSide = '';
%             if j == 1
%                 whichSide = 'left';
%             else
%                 whichSide = 'right';
%             end
            
            for l = 1:2
                condition = '';
                if l == 1
                    condition = 'sham';
                else
                    condition = 'stim';
                end
%                 seed_name = sprintf('%s_%s', seedDirName, whichSide);
%                 seed_name = 'anteriorInsula_left_binarized'

                secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", 'Split Condition', kindOfAnalysis, seed_name, session, condition);
%                 secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", kindOfAnalysis, seedDirName, seed_name, condition);

                cd(fullfile("C:","EXAMPLEPATH","Code","tVNS"))
                load('SecondLevel_One-SampleTTest_ShamStim.mat')

                matlabbatch{1, 1}.spm.stats.factorial_design.dir = cellstr(secondleveldir);
                ims = {};
                for k = 1:length(subjects)
                    firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'SplitConditions_combinedROIs_WeightedMean_Butterworth6', seed_name, subjects{k});

%                     firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'WholeRun_combinedROIs', seed_name, subjects{k});
                    ims{end + 1} = (spm_select('FPList',fullfile(firstleveldir, condition), sprintf('^con.*0%s', contrastN)));
                end
                matlabbatch{1, 1}.spm.stats.factorial_design.des.t1.scans = cellstr(ims');
                spm_jobman('run',matlabbatch);
                clear matlabbatch img


                load('EstimateFirstLevel.mat')
                matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(secondleveldir, "SPM.mat"));
                spm_jobman('run',matlabbatch);
                clear matlabbatch img
                
                
                matlabbatch = [];
                matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(secondleveldir, "SPM.mat"));
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'pos'
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1];
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';
                spm_jobman('run', matlabbatch);

                
            end
        end
    end
% end
