%% 
clear all
secondLevelPairedtTest = 0; 
secondLevelOneSampleTTest = 1;

seedDirPath = fullfile("C:","EXAMPLEPATH","masks");
seedDir = dir(fullfile(seedDirPath));

seedList = {};


% for i = 1:length(seedDir)
%     seed = seedDir(i).name;
%     if length(seed) > 2 
%         seedList{end + 1} = seed;
%     end
% end
% 
% if seedList{end} == 'Others'
% seedList(:,17) = [];
% end



folderPath = fullfile("C:","EXAMPLEPATH","raw");
folder = dir(fullfile(folderPath));

subjects = {};
conditions = {'sham', 'stim'};

for i = 1:length(folder)
    subject = folder(i).name;
    if length(subject) == 4 
        subjects{end + 1} = subject;
    end
end

% seedList = {'posteriorInsula_right_probabilistic', 'anteriorInsula_left_probabilistic', 'posteriorInsula_left_probabilistic', 'MNI_Insula_L', 'MNI_Insula_R', 'MNI_Thalamus_L', 'MNI_Thalamus_R'};
% seedList = {'anteriorInsula_right_probabilistic'};
seedList = {'Ia2', 'Ia3', 'Id4', 'Id6', 'Id7', 'Id8', 'Id9', 'Id10'}


% delete motion subject (and right now also the one without any files)
subjects(:,5) = [];
subjects(:,13) = [];

%% 
for i = 1:length(seedList)
   seedDirName = seedList{i};
   seed_name = seedDirName;
    if secondLevelPairedtTest
        kindOfAnalysis = 'PairedtTest_ShamStim_AnteriorIndividualSeeds_WeightedMean_Butterworth6'
        contrastName = 'WholeRun'
        for j = 1:2
            whichSide = '';
            if j == 1
                whichSide = 'left';
            else
                whichSide = 'right';
            end
            seed_name = sprintf('%s_%s', seedDirName, whichSide);
            
            % secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", 'WholeRun', kindOfAnalysis, seed_name);
            
            secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", kindOfAnalysis, seedDirName, seed_name);

            cd(fullfile("C:","EXAMPLEPATH","Code", "tVNS")
            load('SecondLevel_PairedtTest_ShamStim.mat')

            matlabbatch{1, 1}.spm.stats.factorial_design.dir = cellstr(secondleveldir);
            for k = 1:length(subjects)
                firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'WholeRun_posteriorSeeds_WeightedMean_Butterworth6', seedDirName, seed_name, subjects{k});
                % firstleveldir = fullfile("C:","EXAMPLEPATH", "FirstLevel", 'WholeRun_combinedROIs_WeightedMean_Butterworth6', seed_name, subjects{k});

                matlabbatch{1, 1}.spm.stats.factorial_design.des.pt.pair(k).scans(1,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'sham'), '^con.*01'));
                matlabbatch{1, 1}.spm.stats.factorial_design.des.pt.pair(k).scans(2,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'stim'), '^con.*01'));
            end
            spm_jobman('run',matlabbatch);
            clear matlabbatch img


            load('EstimateFirstLevel.mat')
            matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(secondleveldir, "SPM.mat"));
            spm_jobman('run',matlabbatch);
            clear matlabbatch img
            
            matlabbatch = [];
            matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(secondleveldir, "SPM.mat"));
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'sham-stim'
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1 -1];
            matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = 'stim-sham';
            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [-1 1];
            matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';

            spm_jobman('run', matlabbatch);
        end
    end

    if secondLevelOneSampleTTest
        kindOfAnalysis = 'OneSampleTTest_ShamStim_AnteriorIndividualSeeds_WeightedMean_unfiltered'
        condition = 'sham';
        for j = 1:2
            whichSide = '';
            if j == 1
                whichSide = 'left';
            else
                whichSide = 'right';
            end
            
            for l = 1:2
                condition = '';
                if l == 1
                    condition = 'sham';
                else
                    condition = 'stim';
                end
                seed_name = sprintf('%s_%s', seedDirName, whichSide);

                secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", 'WholeRun', kindOfAnalysis, seed_name, condition);
%                 secondleveldir = fullfile("C:","EXAMPLEPATH","SecondLevel", kindOfAnalysis, seedDirName, seed_name, contrastName, condition);

                cd(fullfile("C:","EXAMPLEPATH","Code", "tVNS")
                load('SecondLevel_One-SampleTTest_ShamStim.mat')

                matlabbatch{1, 1}.spm.stats.factorial_design.dir = cellstr(secondleveldir);
                ims = {};
                for k = 1:length(subjects)
                    % firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'WholeRun_combinedSeeds_WeightedMean_unfiltered', seed_name, subjects{k});

                    firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", 'WholeRun_AnteriorInsulaSeeds_WeightedMean_Butterworth6', seedDirName, seed_name, subjects{k});
                    ims{end + 1} = (spm_select('FPList',fullfile(firstleveldir, condition), '^con.*01'));
                end
                matlabbatch{1, 1}.spm.stats.factorial_design.des.t1.scans = cellstr(ims');
                spm_jobman('run',matlabbatch);
                clear matlabbatch img


                load('EstimateFirstLevel.mat')
                matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(secondleveldir, "SPM.mat"));
                spm_jobman('run',matlabbatch);
                clear matlabbatch img
                
                
                matlabbatch = [];
                matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(secondleveldir, "SPM.mat"));
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'pos'
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1];
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

                matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(secondleveldir, "SPM.mat"));
                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = 'neg'
                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [-1];
                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';
                spm_jobman('run', matlabbatch);

                
            end
        end
    end
end
