% introduce loop over participants and sessions
addpath /usr/local/spm12
rootpath = fullfile("C:","EXAMPLEPATH","raw");

folder = dir(fullfile(rootpath));

subjects = {};
conditions = {'sham', 'stim'};

for i = 1:length(folder)
    subject = folder(i).name;
    if length(subject) == 4 
        subjects{end + 1} = subject;
    end
end

for i = 1:length(subjects)
    subject = subjects{i};
    for j = 1:length(conditions) 
        condition = conditions{j};
        
        subjectFolder = fullfile(rootpath, subject);
        
        try
            fprintf(sprintf('Processing subject %s for condition %s\n', subject, condition));
            movementRP = spm_select('FPList',subjectFolder,sprintf('^rp.*%s_nodu.*txt$', condition));
            regressors_motion = load(movementRP);
            diff_motion = diff([zeros(1,6); regressors_motion]);
            exceeding_movementFP = any((abs(diff_motion(:,1:3) > 2)),2);
            exceeding_movementAbs = any(any((abs(regressors_motion(:,1:3) > 3)),2) + any((abs(regressors_motion(:,4:end)) > 0.0523),2),2);
            regressors_motion(:,7) = or(exceeding_movementFP, exceeding_movementAbs);
            new_filename_rp = fullfile(rootpath, subject, sprintf('edited_rp_atTVNS_%s_%s_nodu', subject, condition));
            save(new_filename_rp, 'regressors_motion', '-ascii');

            if any(regressors_motion(:,7))
                fprintf(sprintf('%s %s: movement parameter exceeds threshold \n', subject, condition));    
            end      
        end
    end
end

fprintf('Done! \n')
