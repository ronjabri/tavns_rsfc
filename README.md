# Modulation of Resting-State Functional Connectivity by Transcutaneous Auricular Vagus Nerve Stimulation
## Description
In our work, a resting-state fMRI functional connectivity analysis was conducted to explore acute changes in the insula seed-based functional connectivity during taVNS administration, compared to sham stimulation. The analyses were based on the dataset from Borgmann et al. (2021), who applied taVNS and sham stimulations simultaneously while acquiring fMRI images and then compared the resting-state activity of the brainstem during stimulation and post-stimulation versus the baseline resting-state. They found the activity of the nucleus of the solitary tract, dorsal raphe nucleus, substantia nigra, and subthalamic nucleus to be modulated by taVNS. Our analyses also aimed to unravel differences between sham stimulation and taVNS administration, but with a new focus: the insula functional connectivity.

Informed by the literature, we separated the insula into the common subdivisions of the bilateral anterior and posterior insula. After validating the seeds, functional connectivity patterns were analyzed across the whole run, encompassing the baseline, stimulation, and poststimulation periods, for taVNS versus sham stimulation conditions. In further analyses, we compared the connectivity during the acute stimulation and post-stimulation periods, respectively, between sham and taVNS. In addition, we investigated the differences between taVNS and sham connectivity patterns of the independent insula subregions from the Jülich Brain Atlas. Exploratory analyses were additionally conducted for NTS and thalamus seeds. 

## Code Structure
All the code was written and tested in a MATLAB (2019b, MathWorks) environment. The code is structured to include all scripts in the root directory, and the batches in a subdirectory. All functions in the Shen directory were written by Shen (2023), and were utilized partly in the conversion step from the residuals after the denoising GLMs to usable functional data.

The main scripts are tVNSProcessing, secondLevelAnalysis and tVNSProcessing_Conditions. With the script tVNSProcessing, all preprocessing and first-level analyses were conducted for the whole runs. With secondLevelAnalysis, all second-level analyses were conducted. With tVNSProcessing_Conditions, the preprocessing and first-level analyses for the split run into baseline, stimulation, and post-stimulation were conducted. All of these scripts utilize batches that are saved in the _batches_ directory. 

All other scripts were used for the creation of visualizations for the thesis, or for quality control steps inbetween the analyses. 

## Authors and acknowledgment
Parts of the code were adapted from scripts of other Psychophysiology and Digital Biomarkers group members and Dr. Saemann of the Neuroimaging Core Unit, who are all affiliated with the Max Planck Institute of Psychiatry. As mentioned in the section **Code Structure**, functions by Shen (2023) were partly utilized.   


## References
Borgmann, D., Rigoux, L., Kuzmanovic, B., Thanarajah, S. E., Münte, T. F., Fenselau, H., & Tittgemeyer, M. (2021). Modulation of fMRI brainstem responses by transcutaneous vagus nerve stimulation. Neuroimage, 244, 118566.

Shen, J. (2023). Tools for NIfTI and ANALYZE image. MATLAB Central File Exchange. Retrieved September 15, 2023, from https://www.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image

