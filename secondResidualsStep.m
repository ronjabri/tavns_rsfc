% Transform double- to single FLOAT images
fullNuisanceDirectory = fullfile(subjectFolder, "FullNuisanceFilteredOut", condition);
cd(fullNuisanceDirectory)

voxel_size = [2 2 3.5];
c = 'rm -r tmp'; system(c);
c = 'mkdir tmp'; system(c);
c = 'cp Res_* tmp'; system(c);
cd tmp
c = 'ls -1 Res_*.nii > listconvert.txt'; system(c);
c = 'ls -1 ../beta*.nii > betalist.txt'; system(c);
img_list = textread('listconvert.txt', '%s', 'delimiter', '\n');
beta_list = textread('betalist.txt', '%s', 'delimiter', '\n');
last_beta = beta_list{size(beta_list,1)};
beta = load_nii(last_beta); beta_img = beta.img;


for n= 1:size(img_list,1)
    tmp = load_nii(img_list{n}); tmp_img = tmp.img;
    tmp.hdr.dime.datatype = [16];  tmp.hdr.dime.bitpix = [32];
    % add beta 
    tmp.img = tmp.img + beta_img;
        
    save_nii(tmp, ['float_32_',img_list{n}],[]);
   
    if isfloat(tmp.img('single')) == 0
        disp ('### Error: tranformation to single_float corrupted ###'); return; 
    end

end
 
disp(['### Successully transformed ',num2str(n),' double to single_floats! ###']);
c = 'rm *.mat'; system(c);
c = 'mv float* ..'; system(c);
%c = 'rm Res_0*'; system(c);
cd ..
