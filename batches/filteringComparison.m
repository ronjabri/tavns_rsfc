% % figure
% % tiledlayout(4,1)
% % % 
% % nexttile
% % plot(mask_seed)
% % title("Original data")
% 
% % bandpass uses a minimum-order filter with a stopband attenuation of 60
% % dB and compensates for the delay introduced by the filter. 
% sig_flt = bandpass(mask_seed, [0.01 0.1], 0.5)
% % nexttile
% % plot(sig_flt)
% % title("Bandpass MATLAB function")
% 
% % higher order filter become too abstract/ deviate extremely from the
% % original signal 
% [b, a] = butter(1, [0.01, 0.1], 'bandpass')
% filtered_signal = filtfilt(b,a,mask_seed);
% % nexttile
% % plot(filtered_signal)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(2, [0.01, 0.1], 'bandpass')
% filtered_signal2 = filtfilt(b,a,mask_seed);
% % nexttile
% % plot(filtered_signal)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% 
% [b, a] = butter(3, [0.01, 0.1], 'bandpass')
% filtered_signal3 = filtfilt(b,a,mask_seed);
% % nexttile
% % plot(filtered_signal3)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(4, [0.01, 0.1], 'bandpass')
% filtered_signal4 = filtfilt(b,a,mask_seed);
% % plot(filtered_signal4)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(5, [0.01, 0.1], 'bandpass')
% filtered_signal5 = filtfilt(b,a,mask_seed);
% % plot(filtered_signal5)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(6, [0.01, 0.1], 'bandpass')
% filtered_signal6 = filtfilt(b,a,mask_seed);
% % plot(filtered_signal6)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(7, [0.01, 0.1], 'bandpass')
% filtered_signal7 = filtfilt(b,a,mask_seed);
% % plot(filtered_signal7)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% [b, a] = butter(8, [0.01, 0.1], 'bandpass')
% filtered_signal8 = filtfilt(b,a,mask_seed);
% % plot(filtered_signal8)
% % title("Minimum-order Butterworth filter Bandpass")
% 
% 
% 
% %% Understand what is happening here:
% % nsample = length(mask_seed); % amount of time-points
% % fs = 1/2;
% % delta = 1/fs;
% % order = 3;      %choose butterworth filter order
% % data = mask_seed;
% % 
% % freq =fs * [0:nsample/2-1]/nsample;
% % 
% % %************ FFT BUTTERWORTH FILTER **************%
% % %%% set up butterworth filtering weights %%%
% % period = nsample/fs;
% % hzpbin = 1/period;
% % flo = 0.01;
% % fhi = 0.10;
% % i = [1:nsample/2+1];
% % r_lo = ((i-1)*hzpbin/flo).^(2*order);
% % factor_lo = r_lo./(1 + r_lo);
% % r_hi = ((i-1)*hzpbin/fhi).^(2*order);
% % factor_hi = 1./(1 + r_hi);
% % for i = nsample/2+2:nsample
% %     factor_lo(i) = factor_lo(nsample-i+2);
% %     factor_hi(i) = factor_hi(nsample-i+2);
% % end
% % 
% % % 
% % %%% plot raw filtered data %%%
% % fftx = fft(data);
% % for i = 1:nsample
% %    fftx_filt(i) = fftx(i) * sqrt(factor_lo(1,i) * factor_hi(1,i));
% % end
% % dataA = ifft(fftx_filt);
% 
% 
% 
% [b, a] = ellip(10,3, 40, [0.01, 0.1]/0.25);
% d = designfilt('bandpassiir', 'FilterOrder', 26, 'PassbandFrequency1', 0.01, 'PassbandFrequency2', 0.1, 'PassbandRipple', 3, 'StopbandAttenuation1', 40, 'StopbandAttenuation2', 40, 'SampleRate', 0.5);
% elliptic_filtered_signal = filtfilt(b,a,mask_seed);
% elliptic_filtered_signal = filtfilt(d, mask_seed);
% 
% 
% figure
% plot(zscore(mask_seed), 'Color',[0,0,1,0.7])
% hold on
% plot(zscore(filtered_signal))
% plot(zscore(filtered_signal2))
% plot(zscore(filtered_signal3))
% plot(zscore(filtered_signal4))
% plot(zscore(filtered_signal5))
% plot(zscore(filtered_signal6))
% plot(zscore(filtered_signal7))
% plot(zscore(filtered_signal8))
% plot(zscore(sig_flt), 'g')
% plot(zscore(PPG_cheby_filter(mask_seed, 0.5)), 'r')
% plot(zscore(bandpassfilter(mask_seed, 0.5, [0.01, 0.1], 12)))
% plot(zscore(elliptic_filtered_signal), 'r')
% legend('Original dataset', 'Butterworth 1st-Order', 'Butterworth 2nd-Order', 'Butterworth 3rd-Order','Butterworth 4th-Order', 'Butterworth 5th-Order', 'Butterworth 6th-Order','Butterworth 7th-Order', 'Butterworth 8th-Order', 'MATLAB Bandpass function', 'Chebyshev filter', 'Fieldtrip Bandpass Filter', 'Elliptic filter')
% 
% function filtered_PPG_signal = PPG_cheby_filter(PPG, sampling_freq)
%     %% Filter w/ bandpass 0.5 - 10 Hz Chebyshev Type II filter
%     % n = order 4
%     % Rs = stopband attenuation 40
%     % Ws = stopband edge frequency [0.5 Hz 10 Hz]
%     % ftype = 'bandpass'
%     % [b,a] = cheby2(n,Rs,Ws,ftype), b & a are transfer function coeffs
%     
%     Ws_Hz = [0.01 0.1]; % 1 Hz = 2pi rad/s; conv to radians/sample by:
%     % stopband edge / (sampling freq/2) pi radians per sample
%     Ws = Ws_Hz / (sampling_freq/2);
% %     [z,p,k] = cheby2(2,40, Ws, 'bandpass');
%     [b,a] = cheby2(12,30, Ws, 'bandpass');
% %     sos = zp2sos(z,p,k);
% %     filtered_PPG_signal = sosfilt(sos,PPG);
%     filtered_PPG_signal = filtfilt(b, a, PPG);
% 
%     
% end
% 
% function [filt] = bandpassfilter(dat,Fs,Fbp,N,type,dir)
% 
% % BANDPASSFILTER filters EEG/MEG data in a specified band
% %
% % Use as
% %   [filt] = bandpassfilter(dat, Fsample, Fbp, N, type, dir)
% % where
% %   dat        data matrix (Nchans X Ntime)
% %   Fsample    sampling frequency in Hz
% %   Fbp        frequency band, specified as [Fhp Flp]
% %   N          optional filter order, default is 4 (but) or 25 (fir)
% %   type       optional filter type, can be
% %                'but' Butterworth IIR filter (default)
% %                'fir' FIR filter using MATLAB fir1 function
% %   dir        optional filter direction, can be
% %                'onepass'         forward filter only
% %                'onepass-reverse' reverse filter only, i.e. backward in time
% %                'twopass'         zero-phase forward and reverse filter (default)
% %
% % Note that a one- or two-pass filter has consequences for the
% % strength of the filter, i.e. a two-pass filter with the same filter
% % order will attenuate the signal twice as strong.
% %
% % See also LOWPASSFILTER, HIGHPASSFILTER
% 
% % Copyright (c) 2003, Robert Oostenveld
% %
% % This file is part of FieldTrip, see http://www.fieldtriptoolbox.org
% % for the documentation and details.
% %
% %    FieldTrip is free software: you can redistribute it and/or modify
% %    it under the terms of the GNU General Public License as published by
% %    the Free Software Foundation, either version 3 of the License, or
% %    (at your option) any later version.
% %
% %    FieldTrip is distributed in the hope that it will be useful,
% %    but WITHOUT ANY WARRANTY; without even the implied warranty of
% %    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% %    GNU General Public License for more details.
% %
% %    You should have received a copy of the GNU General Public License
% %    along with FieldTrip. If not, see <http://www.gnu.org/licenses/>.
% %
% % $Id$
% 
% % set the default filter order later
% if nargin<4
%     N = [];
% end
% 
% % set the default filter type
% if nargin<5
%   type = 'but';
% end
% 
% % set the default filter direction
% if nargin<6
%   dir = 'twopass';
% end
% 
% % Nyquist frequency
% Fn = Fs/2;
% 
% % compute filter coefficients
% switch type
%   case 'but'
%     if isempty(N)
%       N = 4;
%     end
%     [B, A] = butter(N, [min(Fbp)/Fn max(Fbp)/Fn]);
%   case 'fir'
%     if isempty(N)
%       N = 25;
%     end
%     B = fir1(N, [min(Fbp)/Fn max(Fbp)/Fn]);
%     A = 1;
% end
% 
% % apply filter to the data
% switch dir
%   case 'onepass'
%     filt = filter(B, A, dat')';
%   case 'onepass-reverse'
%     dat  = fliplr(dat);
%     filt = filter(B, A, dat')';
%     filt = fliplr(filt);
%   case 'twopass'
%     filt = filtfilt(B, A, dat')';
% end
% end
% 

fc = 2e9;

[zb,pb,kb] = butter(6,2*pi*fc,"s");
[bb,ab] = zp2tf(zb,pb,kb);
[hb,wb] = freqs(bb,ab,4096);

[z2,p2,k2] = cheby2(12,30,2*pi*fc,"s");
[b2,a2] = zp2tf(z2,p2,k2);
[h2,w2] = freqs(b2,a2,4096);

figure
plot([wb w2]/(2e9*pi), ...
    mag2db(abs([hb h2])))
axis([0 5 -45 5])
grid
xlabel("Frequency (GHz)")
ylabel("Attenuation (dB)")
xline(2,'--k')
legend(["Butterworth" "Chebyshev Type II" "Frequency cut-off"])
