folderPath = "/data/tVNS/Ronja_Tryout/raw";
folder = dir(fullfile(folderPath));

subjects = {};

for i = 1:length(folder)
    subject = folder(i).name;
    if length(subject) == 4 
        subjects{end + 1} = subject;
    end
end

seedList = {'posteriorInsula_right', 'anteriorInsula_left', 'anteriorInsula_right','posteriorInsula_left'};

% delete motion subject (and right now also the one without any files)
subjects(:,5) = [];
subjects(:,13) = [];


load('ANOVAdifferentROIpreparationsNew.mat')

seed_name = 'anteriorInsula_right'

secondleveldir = fullfile('/data/tVNS/Ronja_Tryout/SecondLevel', 'ComparisonFilterXTimeSeriesExtraction', seed_name);
firstleveldir = '/data/tVNS/Ronja_Tryout/FirstLevel';
matlabbatch{1, 1}.spm.stats.factorial_design.dir  = cellstr(secondleveldir);

for k = 1:length(subjects)
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(1,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_MeanBinarized_SeedsAfterProbabilisticMap_Butterworth6',sprintf('%s_binarizedAfterProbabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(2,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_MeanBinarized_SeedsAfterProbabilisticMap_Chebyshev',sprintf('%s_binarizedAfterProbabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(3,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_WeightedMeanProbabilistic_Butterworth6',sprintf('%s_probabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(4,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_WeightedMeanProbabilistic_Chebyshev',sprintf('%s_probabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(5,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_binarizedPCA_SeedsAfterProbabilisticMap_Butterworth6',sprintf('%s_binarizedAfterProbabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(6,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_binarizedPCA_SeedsAfterProbabilisticMap_Chebyshev',sprintf('%s_binarizedAfterProbabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(7,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_probabilisticPCA_Butterworth6',sprintf('%s_probabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).scans(8,1) = cellstr(spm_select('FPList',fullfile(firstleveldir, 'WholeRun_combinedROIs_probabilisticPCA_Chebyshev',sprintf('%s_probabilistic', seed_name), subjects{k}, 'sham'), '^con.*01'));
    
    matlabbatch{1, 1}.spm.stats.factorial_design.des.anovaw.fsubject(k).conds = [1,2,3,4,5,6,7,8];
end

% spm_jobman('run',matlabbatch);
% clear matlabbatch img
