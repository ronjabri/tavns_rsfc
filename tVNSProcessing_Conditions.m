nVolumesNoDu  = 357;    

fullPreprocessing = 0;
runNothing = 0;

sliceTiming = 0;
realignment = 0;
coregistration = 0;
normalization = 0;
smoothing = 0;

linearDetrending = 0;
denoising = 0;
fullNuisanceFilteringOut = 0;

if fullPreprocessing
    sliceTiming = 1;
    realignment = 1;
    coregistration = 1;
    normalization = 1;
    smoothing = 1;
    linearDetrending = 1;
    denoising = 1;
    fullNuisanceFilteringOut = 1;
end

timeSeriesSeeds = 0;
firstLevelAnalysisSeed = 1;
contrastEstimation = 1;
pcaROI = 0;


if runNothing
    sliceTiming = 0;
    realignment = 0;
    coregistration = 0;
    normalization = 0;
    smoothing = 0;
    linearDetrending = 0;
    denoising = 0;
    fullNuisanceFilteringOut = 0;
    binarizingMask = 0;
    timeSeriesSeeds = 0;
    firstLevelAnalysisSeed = 0;
    contrastEstimation = 0;
    secondLevelPairedtTest = 0;
end

nBaseline = 57;
nStimulation = 210;
nPostStimulation = 90;
% 
% seedDirPath = fullfile("C:","EXAMPLEPATH","masks");
% seedDir = dir(fullfile(seedDirPath));
% 
% seedList = {};
% 
% for i = 1:length(seedDir)
%     seed = seedDir(i).name;
%     if length(seed) > 2 
%         seedList{end + 1} = seed;
%     end
% end
% 
% if seedList{end} == 'Others'
% seedList(:,17) = [];
% end

% sessionsList = {'baseline', 'stimulation', 'poststimulation'};

seedList = {'anteriorInsula_right_probabilistic', 'posteriorInsula_left_probabilistic', 'posteriorInsula_right_probabilistic', 'anteriorInsula_left_probabilistic', 'MNI_Insula_L', 'MNI_Insula_R', 'MNI_Thalamus_L', 'MNI_Thalamus_R'};
% seedList = {'anteriorInsula_right_binarizedAfterProbabilistic'};
% 
% 

for b = 1:length(seedList)
    seedDirName = seedList{b}
%     seedDirName = 'posteriorInsula_right_binarizedAfterProbabilistic';
    %% Finding the right files

    folderPath = fullfile("C:","EXAMPLEPATH","raw");
    folder = dir(fullfile(folderPath));

    subjects = {};
    conditions = {'sham', 'stim'};

    for i = 1:length(folder)
        subject = folder(i).name;
        if length(subject) == 4 
            subjects{end + 1} = subject;
        end
    end

    % delete motion subject (and right now also the one without any files)
    subjects(:,5) = []
    subjects(:,13) = [];




    %% Preprocessing
% for g = 1:length(sessionsList)
%       session = sessionsList{g};
%       session = 'poststimulation';
      if strcmp(session, 'baseline')
          currentSessionN = nBaseline;
          searchTerm = 'base'
      elseif strcmp(session, 'stimulation')
          currentSessionN = nStimulation;
          searchTerm = 'stim'
      else
          currentSessionN = nPostStimulation;
          searchTerm = 'postst'
      end
    for i = 1:length(subjects)
    subject = subjects{i};
%     subject = subjects{13}
        for j = 1:length(conditions) 
            condition = conditions{j};
%             condition = conditions{2};
            sprintf("Processing subject %s for condition %s", subject, condition)

            subjectFolder = fullfile(folderPath, subject);


            cd(fullfile("C:","EXAMPLEPATH","Code","tVNS")
           % try

                % SLICETIMING
                if sliceTiming
                    load('sliceTiming.mat')
                    img = spm_select('FPList',subjectFolder,sprintf('^%s.*%s.*nii$', searchTerm, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    matlabbatch{1,1}.spm.temporal.st.scans{1,1} = cellstr(ims);
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img
                end

                % REALIGNEMENT
                if realignment
                    load('realignment.mat')
                    img = spm_select('FPList',subjectFolder, sprintf('^a%s.*%s.*nii$', searchTerm, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    matlabbatch{1,1}.spm.spatial.realign.estimate.data{1,1} = cellstr(ims);
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img
                end

                % COREGISTRATION
                if coregistration
                    load('coregistration.mat')
                    refImg = spm_select('FPList',subjectFolder, '^tANAT.*t1.*');
                    img = spm_select('FPList',subjectFolder, sprintf('^a%s.*%s.*nii$', searchTerm, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    sourceImg = ims(1);
                    matlabbatch{1,1}.spm.spatial.coreg.estimate.ref = cellstr(refImg);
                    matlabbatch{1,1}.spm.spatial.coreg.estimate.source = cellstr(sourceImg);
                    matlabbatch{1,1}.spm.spatial.coreg.estimate.other = cellstr(ims(2:end));
                    spm_jobman('initcfg')
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img
                end

                % NORMALIZATION
                if normalization
                    load('normalization.mat')
                    img = spm_select('FPList',subjectFolder, sprintf('^a%s.*%s.*nii$', searchTerm, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    img_align = spm_select('FPList',subjectFolder, '^tANAT.*t1.*');
                    matlabbatch{1,1}.spm.spatial.normalise.estwrite.subj.vol = cellstr(img_align);
                    matlabbatch{1,1}.spm.spatial.normalise.estwrite.subj.resample = cellstr(ims);
                    spm_jobman('initcfg')
                    spm_jobman('run',matlabbatch);
                    %clear matlabbatch img
                end

                % SMOOTHING
                if smoothing
                    load('smoothing.mat')
                    img = spm_select('FPList',subjectFolder,sprintf('^wa%s.*%s.*nii$', searchTerm, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    matlabbatch{1,1}.spm.spatial.smooth.data = cellstr(ims);
                    spm_jobman('initcfg')
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch
                end


                % LINEAR DETRENDING
                if linearDetrending
                    linearDetrendingDir = fullfile(subjectFolder, "LinearDetrending", condition, session);

                    movementRP = spm_select('FPList',subjectFolder,sprintf('^rp_a%s.*%s.*txt$', searchTerm,condition));
                    movementRP = load(movementRP);
                    movement_diff(1,1:6) = 0;      % first row 0, further rows are values of change from t2-t1
                    movement_diff(2:currentSessionN,:) = abs(diff(movementRP));
                    all_motion_nuisance = [movementRP, movement_diff];
                    all_motion_nuisance_z = zscore(all_motion_nuisance);
                    save(fullfile(subjectFolder, "motionParameterForResidualization.txt"),'all_motion_nuisance_z', '-ascii') 

                    load('LinearDetrendingNew.mat')
                    img = spm_select('FPList',subjectFolder,sprintf('^swa%s.*%s.*nii$', session, condition));
                    ims = strcat(repmat(img,currentSessionN,1), ',',""+(1:currentSessionN).');
                    matlabbatch{1,1}.spm.stats.fmri_spec.dir = cellstr(linearDetrendingDir);
                    matlabbatch{1,1}.spm.stats.fmri_spec.sess.scans = cellstr(ims);
                    matlabbatch{1,1}.spm.stats.fmri_spec.sess.multi_reg = cellstr(fullfile(subjectFolder, "motionParameterForResidualization.txt")); 
                    matlabbatch{1,1}.spm.stats.fmri_spec.mthresh = cellstr('-Inf');
                    spm_jobman('run',matlabbatch);


                    load('LinearDetrendingResiduals.mat')
                    matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(linearDetrendingDir, "SPM.mat"));
                    matlabbatch{1,1}.spm.stats.fmri_est.write_residuals = 1;
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img

                    residualsInbetweenStep(linearDetrendingDir)
                end

                if denoising
                % MARSBAR: get CSF and white matter regressors
                    image_array = spm_select('FPList',fullfile(subjectFolder, "LinearDetrending", condition, session),'^float_32_Res_.*nii$');
                    csf_roi = 'csf_roi70_roi.mat';
                    wm_roi = 'wm_roi.mat';
                    roi_files = {csf_roi;wm_roi};
                    rois = maroi('load_cell',roi_files);
                    mY = get_marsy(rois{:},image_array,'mean');
                    mask_all_cell = region_data(mY);
                    csf = double(mask_all_cell{1});
                    wm = double(mask_all_cell{2});
                    mask_seed = summary_data(mY); 

                    % do a PCA to extract components that account for the majority of variance
                    % in CSF fluctuations
                    % CSF-regressors
%                     figure,plot(mean(csf')) % mean csf change
                    [coeff_csf, scores_csf, latent_csf,~,explained] = pca(csf(:,:)); 
                    %[coeff_csf, scores_csf, latent_csf,~,explained] = pca(csf); 

                    csf_pc1=scores_csf(:,1); % first 3 principal components for cerebrospinal fluid regressor
                    csf_pc2=scores_csf(:,2);
                    csf_pc3=scores_csf(:,3);
                    csf_pc4=scores_csf(:,4);
                    csf_pc5=scores_csf(:,5);
                    expl_var_csf(1)=latent_csf(1)/sum(latent_csf); % explained variance by first 3 principal components
                    expl_var_csf(2)=latent_csf(2)/sum(latent_csf);
                    expl_var_csf(3)=latent_csf(3)/sum(latent_csf);
                    expl_var_csf(4)=latent_csf(4)/sum(latent_csf);
                    expl_var_csf(5)=latent_csf(5)/sum(latent_csf);

                    %WM-regressors
                    % figure, plot(mean(wm')) % mean wm change
                    [coeff_wm,scores_wm,latent_wm]=pca(wm(:,:));
                    %[coeff_wm,scores_wm,latent_wm]=pca(wm);
                    wm_pc1=scores_wm(:,1); % first 3 principal components for white matter regressor
                    wm_pc2=scores_wm(:,2);
                    wm_pc3=scores_wm(:,3);
                    wm_pc4=scores_wm(:,4);
                    wm_pc5=scores_wm(:,5);
                    expl_var_wm(1)=latent_wm(1)/sum(latent_wm); % explained variance by first 3 principal components
                    expl_var_wm(2)=latent_wm(2)/sum(latent_wm);
                    expl_var_wm(3)=latent_wm(3)/sum(latent_wm);
                    expl_var_wm(4)=latent_wm(4)/sum(latent_wm);
                    expl_var_wm(5)=latent_wm(5)/sum(latent_wm);

                    % save explained variance in expl_var
                    expl_var(1:5) = expl_var_csf;
                    expl_var(6:10) = expl_var_wm;

                    % Creating a txt file with all regressors (i.e., movement parameters, CSF,
                    % white matter)

                    nuisance = [csf_pc1, csf_pc2, csf_pc3, csf_pc4, csf_pc5, wm_pc1, wm_pc2, wm_pc3, wm_pc4, wm_pc5];
                    all_nuisance_diff(1,1:10) = 0;      % first row 0, further rows are values of change from t2-t1
                    all_nuisance_diff(2:currentSessionN,:) = abs(diff(nuisance));
                    all_nuisance = [nuisance, all_nuisance_diff];
                    nuisance_path_txt = fullfile(subjectFolder, sprintf("nuisance20regressors_%s_%s.txt", condition, session));
                    nuisance_path_mat = fullfile(subjectFolder, sprintf("nuisance20regressors_%s_%s.mat", condition, session));


                    %    % zscore matrix
                    all_nuisance_z = zscore(all_nuisance);

                    save(nuisance_path_mat, "all_nuisance_z");
                    save(nuisance_path_txt, "all_nuisance_z", "-ascii");
                end 

                if fullNuisanceFilteringOut
                    nuisanceFilteringOutDir = fullfile(subjectFolder, "FullNuisanceFilteredOut", condition, session);

                    load('LinearDetrendingNew.mat')
                    ims = spm_select('FPList',linearDetrendingDir, sprintf('^float.*nii$', condition));
                    matlabbatch{1,1}.spm.stats.fmri_spec.dir = cellstr(nuisanceFilteringOutDir);
                    matlabbatch{1,1}.spm.stats.fmri_spec.sess.scans = cellstr(ims);
                    matlabbatch{1,1}.spm.stats.fmri_spec.sess.multi_reg = cellstr(spm_select('FPList',subjectFolder,sprintf("nuisance20regressors_%s_%s.txt", condition, session))); 
                    matlabbatch{1,1}.spm.stats.fmri_spec.mthresh = cellstr('-Inf');
                    spm_jobman('run',matlabbatch);


                    load('LinearDetrendingResiduals.mat')
                    matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(nuisanceFilteringOutDir, "SPM.mat"));
                    matlabbatch{1,1}.spm.stats.fmri_est.write_residuals = 1;
                    spm_jobman('run',matlabbatch); 
                    clear matlabbatch img

                    residualsInbetweenStep(nuisanceFilteringOutDir)
                end




                if timeSeriesSeeds 
                    mask_dir = fullfile("C:","EXAMPLEPATH","masks");
%                     seedDirName = 'posteriorInsula_right_binarized';

                    image_array = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition, session), '^float.*nii$');
%                     maskLeftPath     = spm_select('FPList',fullfile(mask_dir, seedDirName), sprintf('probabilistic.*%s_left.*mat$', seedDirName));
%                     maskRightPath    = spm_select('FPList',fullfile(mask_dir, seedDirName), sprintf('probabilistic.*%s_right.*mat$', seedDirName));
                    
                    maskLeftPath     = spm_select('FPList',fullfile(mask_dir, "Others"), sprintf('^%s.*mat$', seedDirName));
%                     maskRightPath    = spm_select('FPList',fullfile(mask_dir, seedDirName), sprintf('probabilistic.*%s_right.*mat$', seedDirName));

                    maskLeft = load(maskLeftPath);
%                     maskRight = load(maskRightPath);
                    rois = maskLeft.roi;
%                     rois{2} = maskRight.roi;
%                     mY = get_marsy(rois{:},image_array,'mean');
                    mY = get_marsy(rois,image_array,'mean');
                    mask_all_cell = region_data(mY);
%                     Left = double(mask_all_cell{1});
                    mask_seed = summary_data(mY);  
                    
                    [b, a] = butter(6, [0.01, 0.1], 'bandpass');
                    mask_seed = filtfilt(b,a,mask_seed);
%                 mask_seed = PPG_cheby_filter(mask_seed, 0.5)

                    save(fullfile(subjectFolder, sprintf('TimeSeries_%s_%s_%s.txt', session, condition, seedDirName)), 'mask_seed', '-ascii');

                end

                if firstLevelAnalysisSeed
                    kindOfAnalysis = 'SplitConditions_combinedROIs_WeightedMean_Butterworth6'
%                     seed = mask_seed(:,k);
                    seed_name = seedDirName;
                    firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", kindOfAnalysis, seedDirName, subject, condition);

                    load('FirstLevelSeedConditions.mat')
                    ims = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition), '^float.*nii$');

                    matlabbatch{1,1}.spm.stats.fmri_spec.dir = cellstr(firstleveldir);
                    
                    ims = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition, 'baseline'), '^float.*nii$');
                    regressValBaseline = load(fullfile(subjectFolder, sprintf('TimeSeries_baseline_%s_%s.txt', condition, seedDirName)));
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(1).scans = cellstr(ims);
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(1).regress.name  = 'Baseline';
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(1).regress.val  = regressValBaseline;
                    
                    ims = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition, 'stimulation'), '^float.*nii$');
                    regressValStimulation = load(fullfile(subjectFolder, sprintf('TimeSeries_stimulation_%s_%s.txt', condition, seedDirName)));
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(2).scans = cellstr(ims);
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(2).regress.name  = 'Stimulation';
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(2).regress.val  = regressValStimulation;
                    
                    ims = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition, 'poststimulation'), '^float.*nii$');
                    regressValPostStim = load(fullfile(subjectFolder, sprintf('TimeSeries_poststimulation_%s_%s.txt', condition, seedDirName)));
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(3).scans = cellstr(ims);
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(3).regress.name  = 'Poststimulation';
                    matlabbatch{1, 1}.spm.stats.fmri_spec.sess(3).regress.val  = regressValPostStim;
                    
                    
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img


                    load('EstimateFirstLevel.mat')
                    matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(firstleveldir, "SPM.mat"));
                    spm_jobman('run',matlabbatch);
                    clear matlabbatch img
                end

                if contrastEstimation
                    kindOfAnalysis = 'SplitConditions_combinedROIs_WeightedMean_Butterworth6'
%                
                    seed_name = seedDirName;
%                         firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", kindOfAnalysis, seedDirName, seed_name, subject, condition);
                    firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", kindOfAnalysis, seedDirName, subject, condition);

                    load(fullfile(firstleveldir, 'SPM.mat'))
                    SPM.xCon = [];


                    matlabbatch = [];
                    matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(firstleveldir, "SPM.mat"));
                    matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'Baseline positive';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1 0 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = 'Baseline negative';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [-1 0 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.name = 'Stimulation positive';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.convec = [0 1 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.name = 'Stimulation negative';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.convec = [0 -1 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.name = 'Poststimulation positive';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.convec = [0 0 1 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.name = 'Poststimulation negative';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.convec = [0 0 -1 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.name = 'Stimulation > Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.convec = [-1 1 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.name = 'Stimulation < Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.convec = [1 -1 0 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.name = 'Poststimulation > Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.convec = [-1 0 1 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.name = 'Poststimulation < Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.convec = [1 0 -1 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.sessrep = 'none';
                    
                    matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.name = 'Stimulation & Poststimulation > Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.convec = [-1 0.5 0.5 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.sessrep = 'none';

                    matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.name = 'Stimulation & Poststimulation < Baseline';
                    matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.convec = [1 -0.5 -0.5 0 0 0];
                    matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.sessrep = 'none';

                    spm_jobman('run', matlabbatch);
%                     end
                end

                if pcaROI
                mask_dir = fullfile("C:","EXAMPLEPATH","masks");
                image_array = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition), '^float.*nii$');
                roiName = seedDirName;
                combinedRoi = fullfile(mask_dir, 'Others', sprintf('%s_roi.mat', roiName));
                rois = load(combinedRoi);
                mY = get_marsy(rois.roi,image_array,'wtmean');
                mask_all_cell = region_data(mY);
                combinedROI = double(mask_all_cell{1});
                mask_seed = summary_data(mY); 
                
                [b, a] = butter(6, [0.01, 0.1], 'bandpass')
                mask_seed = filtfilt(b,a,mask_seed);
%                 mask_seed = PPG_cheby_filter(mask_seed, 0.5)


                % do a PCA to extract components that account for the majority of variance
                % in CSF fluctuations
                % CSF-regressors
                figure,plot(mean(combinedROI')) % mean csf change
                [coeff_roi, scores_roi, latent_roi,~,explained] = pca(combinedROI(:,:)); 
                %[coeff_csf, scores_csf, latent_csf,~,explained] = pca(csf); 

                roi_pc1=scores_roi(:,1); % first 3 principal components for cerebrospinal fluid regressor
                roi_pc2=scores_roi(:,2);
                roi_pc3=scores_roi(:,3);
                roi_pc4=scores_roi(:,4);
                roi_pc5=scores_roi(:,5);
                expl_var_roi(1)=latent_roi(1)/sum(latent_roi); % explained variance by first 3 principal components
                expl_var_roi(2)=latent_roi(2)/sum(latent_roi);
                expl_var_roi(3)=latent_roi(3)/sum(latent_roi);
                expl_var_roi(4)=latent_roi(4)/sum(latent_roi);
                expl_var_roi(5)=latent_roi(5)/sum(latent_roi);
                
                seed_name = roiName;
                kindOfAnalysis = 'SplitConditions_combinedROIs_MeanBinarized_Butterworth6'
                firstleveldir = fullfile("C:","EXAMPLEPATH","FirstLevel", kindOfAnalysis, seed_name, subject, condition);

                load('FirstLevelSeedConditions.mat')
                ims = spm_select('FPList',fullfile(subjectFolder, 'FullNuisanceFilteredOut', condition), '^float.*nii$');

                matlabbatch{1,1}.spm.stats.fmri_spec.dir = cellstr(firstleveldir);
                matlabbatch{1,1}.spm.stats.fmri_spec.sess.scans = cellstr(ims);
                matlabbatch{1,1}.spm.stats.fmri_spec.sess.regress.name  = seed_name;
                matlabbatch{1,1}.spm.stats.fmri_spec.sess.regress.val  = roi_pc1;
                spm_jobman('run',matlabbatch);
                clear matlabbatch img


                load('EstimateFirstLevel.mat')
                matlabbatch{1,1}.spm.stats.fmri_est.spmmat = cellstr(fullfile(firstleveldir, "SPM.mat"));
                spm_jobman('run',matlabbatch);
                clear matlabbatch img
                
                matlabbatch = [];
                matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(firstleveldir, "SPM.mat"));
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = 'Baseline positive';
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1 0 0 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = 'Baseline negative';
                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [-1 0 0 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';
                
                matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.name = 'Stimulation positive';
                matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.convec = [0 1 0 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.sessrep = 'none';

                matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.name = 'Stimulation negative';
                matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.convec = [0 -1 0 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.sessrep = 'none';
                
                matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.name = 'Poststimulation positive';
                matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.convec = [0 0 1 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.sessrep = 'none';

                matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.name = 'Poststimulation negative';
                matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.convec = [0 0 -1 0 0 0];
                matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.sessrep = 'none';

                spm_jobman('run', matlabbatch);
            end


    %         catch
    %             continue
%             end
        end
    end
end
% end

function filtered_PPG_signal = PPG_cheby_filter(PPG, sampling_freq)
    %% Filter w/ bandpass 0.5 - 10 Hz Chebyshev Type II filter
    % n = order 4
    % Rs = stopband attenuation 40
    % Ws = stopband edge frequency [0.5 Hz 10 Hz]
    % ftype = 'bandpass'
    % [b,a] = cheby2(n,Rs,Ws,ftype), b & a are transfer function coeffs
    
    Ws_Hz = [0.01 0.1]; % 1 Hz = 2pi rad/s; conv to radians/sample by:
    % stopband edge / (sampling freq/2) pi radians per sample
    Ws = Ws_Hz / (sampling_freq/2);
%     [z,p,k] = cheby2(2,40, Ws, 'bandpass');
    [b,a] = cheby2(12,30, Ws, 'bandpass');
%     sos = zp2sos(z,p,k);
%     filtered_PPG_signal = sosfilt(sos,PPG);
    filtered_PPG_signal = filtfilt(b, a, PPG);

    
end
