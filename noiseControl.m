nVolumesNoDu  = 357;    

fullPreprocessing = 0;
runNothing = 0;

sliceTiming = 0;
realignment = 0;
coregistration = 0;
normalization = 0;
smoothing = 0;

linearDetrending = 0;
denoising = 0;
fullNuisanceFilteringOut = 0;

if fullPreprocessing
    sliceTiming = 1;
    realignment = 1;
    coregistration = 1;
    normalization = 1;
    smoothing = 1;
    linearDetrending = 1;
    denoising = 1;
    fullNuisanceFilteringOut = 1;
end

timeSeriesSeeds = 1;
firstLevelAnalysisSeed = 1;
contrastEstimation = 1;
pcaROI = 0;


if runNothing
    sliceTiming = 0;
    realignment = 0;
    coregistration = 0;
    normalization = 0;
    smoothing = 0;
    linearDetrending = 0;
    denoising = 0;
    fullNuisanceFilteringOut = 0;
    binarizingMask = 0;
    timeSeriesSeeds = 0;
    firstLevelAnalysisSeed = 0;
    contrastEstimation = 0;
    secondLevelPairedtTest = 0;
end



folderPath = fullfile("C:","EXAMPLEPATH","raw");
folder = dir(fullfile(folderPath));

subjects = {};

    for i = 1:length(folder)
        subject = folder(i).name;
        if length(subject) == 4 
            subjects{end + 1} = subject;
        end
    end

    % delete motion subject (and right now also the one without any files)
    % subjects(:,5) = []
    % subjects(:,13) = [];

    subjects = {'4987', '5183', '8604'}


    %% Preprocessing

    for i = 1:length(subjects)
    subject = subjects{i};

            subjectFolder = fullfile(folderPath, subject);


            cd(fullfile("C:","EXAMPLEPATH","Code","tVNS"))

                if contrastEstimation
                    
                        noisedir = fullfile("C:","EXAMPLEPATH","raw",subject, 'FullNuisanceFilteredOut_new', 'sham');

                        load(fullfile(noisedir, 'SPM.mat'))
                        SPM.xCon = [];
                        
                        matlabbatch = [];
                        matlabbatch{1,1}.spm.stats.con.spmmat  = cellstr(fullfile(noisedir, "SPM.mat"));
                        matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.name = '1';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.convec = [1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 1}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.name = '2';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.convec = [0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 2}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.name = '3';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.convec = [0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 3}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.name = '4';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.convec = [0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 4}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.name = '5';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.convec = [0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 5}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.name = '6';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.convec = [0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 6}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.name = '7';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.convec = [0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 7}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.name = '8';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.convec = [0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 8}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.name = '9';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.convec = [0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 9}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.name = '10';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.convec = [0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 10}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.name = '11';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 11}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.name = '12';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 12}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 13}.tcon.name = '13';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 13}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 13}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 14}.tcon.name = '14';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 14}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 14}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 15}.tcon.name = '15';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 15}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 15}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 16}.tcon.name = '16';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 16}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 16}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 17}.tcon.name = '17';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 17}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 17}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 18}.tcon.name = '18';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 18}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 18}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 19}.tcon.name = '19';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 19}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 19}.tcon.sessrep = 'none';

                        matlabbatch{1,1}.spm.stats.con.consess{1, 20}.tcon.name = '20';
                        matlabbatch{1,1}.spm.stats.con.consess{1, 20}.tcon.convec = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1];
                        matlabbatch{1,1}.spm.stats.con.consess{1, 20}.tcon.sessrep = 'none';

                        spm_jobman('run', matlabbatch);
                    % end
                end

                


    %         catch
    %             continue
    %         end
        end
   
